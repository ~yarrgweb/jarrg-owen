package com.tedpearson.ypp.market;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.prefs.*;

/**
*	ControlPanel is a simple management utility that sets
*	a preference for whether the PCTB client is to launch or not.
*/
public class ControlPanel extends JFrame {
	public static void main(String[] args) {
		new ControlPanel();
	}
	
	public ControlPanel() {
		super("PCTB Control Panel");
		final Preferences prefs = Preferences.userNodeForPackage(getClass());
		final JCheckBox toPCTB = new JCheckBox("Upload to PCTB?", prefs.getBoolean("uploadToPCTB", true));
		final JCheckBox toYarrg = new JCheckBox("Upload to Yarrg?", prefs.getBoolean("uploadToYarrg", true));

		final JRadioButton live = new JRadioButton("Use live servers");
		final JRadioButton testing = new JRadioButton("Use testing servers");
		
		live.setSelected(prefs.getBoolean("useLiveServers", true));
		testing.setSelected(!prefs.getBoolean("useLiveServers", true));

		ButtonGroup liveortest = new ButtonGroup();
		liveortest.add(live);
		liveortest.add(testing);

		String version_label = " version: " +
		    com.tedpearson.ypp.market.Version.version;
		JLabel version = new JLabel(version_label);

		setLayout(new GridLayout(6,1));
		add(toPCTB);
		add(toYarrg);
		add(live);
		add(testing);
		add(version);

		final int exitstatus = Integer.parseInt(System.getProperty("com.tedpearson.ypp.market.controlpanel.exitstatus", "0"));

		JButton but = new JButton("Save options");
		add(but);
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prefs.putBoolean("launchAtStartup", true);
				prefs.putBoolean("uploadToPCTB", toPCTB.isSelected());
				prefs.putBoolean("uploadToYarrg", toYarrg.isSelected());
				prefs.putBoolean("useLiveServers", live.isSelected());
				System.exit(exitstatus);
			}
		});
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setSize(getWidth() + 10, getHeight() + 10);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getRootPane().setDefaultButton(but);
	}
}
