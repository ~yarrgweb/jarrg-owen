!include FileFunc.nsh
!insertmacro GetParent

Name "JPCTB"
OutFile "jpctb-setup.exe"
InstallDir "$LOCALAPPDATA\JPCTB"
RequestExecutionLevel user

var YPPDIR
var YPPUNINST
var JRE

Page directory
Page instfiles

UninstPage instfiles

Section ""
Call FindJRE
Call CheckYPP
Call CopyJRE
Call YPPShortcuts
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\JPCTB" \
"DisplayName" "JPCTB - Java Pirate Commodity Trader with Bleach"
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\JPCTB" \
"UninstallString" "$\"$INSTDIR\uninstall-jpctb.exe$\""
WriteUninstaller "$INSTDIR\uninstall-jpctb.exe"
SectionEnd

Section "un."
RMDir /r $INSTDIR\jre
Delete $INSTDIR\uninstall-jpctb.exe
RMDIR $INSTDIR

Delete "$SMPROGRAMS\JPCTB.lnk"
Delete "$SMPROGRAMS\JPCTB Control Panel.lnk"
Delete "$DESKTOP\JPCTB.lnk"

DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\JPCTB"

SectionEnd

Function CheckYPP
ClearErrors
ReadRegStr $YPPUNINST HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\Puzzle Pirates" "UninstallString" 
IfErrors 0 YPPFound
Abort "Couldn't find YPP installation.  Bailing out."
YPPFound:
${GetParent} $YPPUNINST $YPPDIR
IfFileExists $YPPDIR\java 0 NoYPPJ15
Abort "YPP currently using internal Java 1.5.  You will need to uninstall Puzzle Pirates and then reinstall it before rerunning this installer."
NoYPPJ15:
IfFileExists $YPPDIR\java_vm 0 NOYPPJ16
Abort "YPP currently using internal Java 1.6.  You will need to uninstall Puzzle Pirates and then reinstall it before rerunning this installer."
NoYPPJ16:
DetailPrint "Found YPP in $YPPDIR"
FunctionEnd

Function FindJRE
ClearErrors
ReadRegStr $R1 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" "CurrentVersion"
ReadRegStr $R0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment\$R1" "JavaHome"
IfErrors 0 JreFound
Abort "Couldn't find your Sun Java VM.  Bailing out."
JreFound:
StrCpy $JRE $R0
DetailPrint "Found Java in $JRE"
FunctionEnd

Function CopyJRE
CreateDirectory $INSTDIR\jre
CopyFiles $JRE\*.* $INSTDIR\jre
File /oname=$INSTDIR\jre\lib\ext\PCTB-Uploader.jar PCTB-Uploader.jar
File /oname=$INSTDIR\jre\lib\ext\PCTB-ControlPanel.jar PCTB-ControlPanel.jar
File /oname=$INSTDIR\jre\lib\accessibility.properties accessibility.properties
FunctionEnd

Function YPPShortcuts
SetOutPath $YPPDIR
CreateShortCut "$DESKTOP\JPCTB.lnk" "$INSTDIR\jre\bin\javaw.exe" \
"-jar getdown-dop.jar ." "$YPPDIR\app_icon.ico" 0 SW_SHOWNORMAL
CreateShortCut "$SMPROGRAMS\JPCTB.lnk" "$INSTDIR\jre\bin\javaw.exe" \
"-jar getdown-dop.jar ." "$YPPDIR\app_icon.ico" 0 SW_SHOWNORMAL
CreateShortCut "$SMPROGRAMS\JPCTB Control Panel.lnk" \
"$INSTDIR\jre\bin\javaw.exe" "com.tedpearson.ypp.market.ControlPanel"
FunctionEnd
